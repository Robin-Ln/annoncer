/*
 * Rédigé par Robin LOUARN
 */

package fr.louarn.pattern;

import java.util.function.Consumer;

/**
 * Obset qui associe un oberserver à une action
 *
 * @param <E> type générique qui représente un observer
 * @param <A> type générique qui représente un observable
 */
public class Registre<E extends IObserver, A extends IObservable> {

    /**
     * Elément observateur
     */
    private E observer;

    /**
     * Action associer à l'observeur
     */
    private Consumer<A> consumer;

    /**
     * Constructeur de la classe Registre
     *
     * @param observer objet qui hérite de la classe IObserver
     * @param consumer objet qui hérite de la classe IObservable
     */
    public Registre(E observer, Consumer<A> consumer) {
        this.observer = observer;
        this.consumer = consumer;
    }

    /*
     * Accesseurs
     */

    public E getObserver() {
        return observer;
    }

    public void setObserver(E observer) {
        this.observer = observer;
    }

    public Consumer<A> getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer<A> consumer) {
        this.consumer = consumer;
    }
}
