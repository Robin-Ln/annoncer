/*
 * Rédigé par Robin LOUARN
 */

package fr.louarn.pattern;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Classe qui permet la gestion des abonements
 */
public class Annonceur {

    /**
     * Map qui stock la liste des regostre en fonction d'un évènement
     */
    private Map<AbstractEvent, List<Registre>> eventMap;

    /**
     * Unique instance de la classe Annonceur
     */
    private static Annonceur annonceur;

    /**
     * Constructeur de la classe
     */
    private Annonceur() {
        eventMap = new HashMap<>();
    }

    /**
     * Permet la création de singleton
     *
     * @return une instance de la classe Annonceur
     */
    public static Annonceur getAnnonceur() {
        if (Objects.isNull(annonceur)) {
            return new Annonceur();
        }
        return annonceur;
    }


    /**
     * Permet à un observer de souscrire à un évènement
     *
     * @param event    évènement généré par l'observable
     * @param observer objet qui hérite de la classe IObserver
     * @param consumer objet qui hérite de la classe IObservable
     * @param <E>      type générique qui représente un observer
     * @param <A>      type générique qui représente un observable
     */
    public <E extends IObserver, A extends IObservable> void suscribe(AbstractEvent<A> event, E observer, Consumer<A> consumer) {
        List<Registre> registres = eventMap.get(event);

        if (Objects.isNull(registres)) {
            registres = new ArrayList<>();
        }

        registres.add(new Registre<E, A>(observer, consumer));
        eventMap.put(event, registres);
    }

    /**
     * Permet à un observer de se désincrire d'un évènement
     *
     * @param event    évènement associé à l'observeur
     * @param observer objet qui hérite de la classe IObserver
     */
    public void unSuscribe(AbstractEvent event, IObserver observer) {
        List<Registre> registres = eventMap.get(event)
                .stream()
                .filter(registre -> !registre.getObserver().equals(observer))
                .collect(Collectors.toList());

        eventMap.put(event, registres);
    }

    /**
     * Permet à l'observable de notifier l'observer
     *
     * @param event évènement créé par l'observer
     */
    public void notify(AbstractEvent event) {
        eventMap.get(event).forEach(registre -> registre.getConsumer().accept(event.getObservable()));
    }

    /*
     * Accesseurs
     */

    public Map<AbstractEvent, List<Registre>> getEventMap() {
        return eventMap;
    }

    public void setEventMap(Map<AbstractEvent, List<Registre>> eventMap) {
        this.eventMap = eventMap;
    }
}
