/*
 * Rédigé par Robin LOUARN
 */

package fr.louarn.pattern;

/**
 * Class abstraite qui représente un évènement
 *
 * @param <A> Type générique qui représente un objet observable
 */
public abstract class AbstractEvent<A extends IObservable> {

    /**
     * observable qui à généré l'évènemnt
     */
    private A observable;

    /**
     * Constructeur de la classe AbstractEvent
     *
     * @param observable objet qui hérite de la classe IObservable
     */
    public AbstractEvent(A observable) {
        this.observable = observable;
    }

    /*
     * Accesseurs
     */

    public A getObservable() {
        return observable;
    }

    public void setObservable(A observable) {
        this.observable = observable;
    }
}
