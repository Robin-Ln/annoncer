/*
 * Rédigé par Robin LOUARN
 */

package fr.louarn.pattern;

public class Event extends AbstractEvent<Observable> {

    public Event(Observable observable) {
        super(observable);
    }
}
