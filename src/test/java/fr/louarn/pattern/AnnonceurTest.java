/*
 * Rédigé par Robin LOUARN
 */

package fr.louarn.pattern;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnnonceurTest {

    private Annonceur annonceur;
    private Boolean notified;
    private Observer observer;
    private Event event;


    @BeforeEach
    void init() {
        this.notified = false;
        this.annonceur = Annonceur.getAnnonceur();
        this.observer = new Observer();
        this.event = new Event(new Observable());
    }

    @Test
    void testNotify() {
        annonceur.suscribe(event, observer, iObservable -> notified = true);
        annonceur.notify(event);
        assertTrue(notified);
        assertEquals(annonceur.getEventMap().size(),1);
    }

    @Test
    void testUnSuscribe() {
        annonceur.suscribe(event, observer, iObservable -> notified = true);
        annonceur.unSuscribe(event,observer);
        annonceur.notify(event);
        assertFalse(notified);
        assertEquals(annonceur.getEventMap().get(event).size(),0);
    }
}
